<?php

declare(strict_types=1);

namespace App\Http\Controllers;


use App\Modules\Product\Application\ProductService;
use App\Modules\Product\ReadModel\ProductNotFoundException;
use App\Modules\Product\ReadModel\ProductReadModel;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    private ProductService $productService;
    private ProductReadModel $productReadModel;

    public function __construct(ProductService $productService, ProductReadModel $productReadModel)
    {
        $this->productService = $productService;
        $this->productReadModel = $productReadModel;
    }

    public function create(Request $request): Response
    {
        try {
            $command = CommandFactory::buildCreateProduct($request);
            $productId = $this->productService->create($command);

            $productDTO = $this->productReadModel->getProduct($productId);
            return response()->json(
                [
                    'id' => $productDTO->getId(),
                    'name' => $productDTO->getName(),
                    'price' => $productDTO->getPrice()
                ]
            );
        } catch (ProductNotFoundException $exception){
            return response()->json(null, 404);
        }
    }
}
