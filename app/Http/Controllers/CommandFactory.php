<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Modules\Product\Application\Command\CreateProduct;
use Illuminate\Http\Request;

class CommandFactory
{
    public static function buildCreateProduct(Request $request): CreateProduct
    {
        $name = $request->get('name');
        $price = $request->get('price');

        return new CreateProduct($name, $price);
    }
}
