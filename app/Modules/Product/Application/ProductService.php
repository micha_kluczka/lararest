<?php

declare(strict_types=1);

namespace App\Modules\Product\Application;

use App\Modules\Product\Application\Command\CreateProduct;
use App\Modules\Product\DomainModel\ProductCollectionFactory;

class ProductService
{
    private ProductCollectionFactory $collectionFactory;

    public function __construct(ProductCollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    public function create(CreateProduct $command): int
    {
        return $this->collectionFactory
            ->createCollection()
            ->addProduct($command->getName(), $command->getPriceAmount());
    }
}
