<?php

declare(strict_types=1);

namespace App\Modules\Product\ReadModel;

interface ProductReadModelRepository
{
    /**
     * @throws ProductNotFoundException
     */
    public function getProductDTO(int $id): ProductDTO;
}
