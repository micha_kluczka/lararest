<?php

declare(strict_types=1);

namespace App\Modules\Product\ReadModel;

class ProductReadModel
{
    private ProductReadModelRepository $repository;

    public function __construct(ProductReadModelRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @throws ProductNotFoundException
     */
    public function getProduct(int $id): ProductDTO
    {
        return $this->repository->getProductDTO($id);
    }
}
