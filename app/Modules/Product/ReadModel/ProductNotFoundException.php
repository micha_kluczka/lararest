<?php

declare(strict_types=1);

namespace App\Modules\Product\ReadModel;

use Exception;

class ProductNotFoundException extends Exception
{

}