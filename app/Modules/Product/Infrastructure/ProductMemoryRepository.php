<?php

declare(strict_types=1);

namespace App\Modules\Product\Infrastructure;

use App\Modules\Product\DomainModel\ProductRepository;
use App\Modules\Product\ReadModel\ProductDTO;
use App\Modules\Product\ReadModel\ProductNotFoundException;
use App\Modules\Product\ReadModel\ProductReadModelRepository;
use Money\Money;

class ProductMemoryRepository implements ProductRepository, ProductReadModelRepository
{
    private static array $storage = [];

    public function addProduct(string $name, Money $price): int
    {
        $nextId = mt_rand(1, 20);

        self::$storage[$nextId] = [
            'id' => (string)$nextId,
            'name' => $name,
            'price' => $price,
        ];

        return $nextId;
    }

    /**
     * @throws ProductNotFoundException
     */
    public function getProductDTO(int $id): ProductDTO
    {
        $data = self::$storage[$id] ?? null;

        if ($data === null) {
            throw new ProductNotFoundException();
        }

        return ProductDTO::fromStorage($data);
    }
}