<?php

declare(strict_types=1);

namespace App\Modules\Product\DomainModel;

use Money\Money;

class ProductCollection
{
    private ProductRepository $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function addProduct(string $name, int $priceAmount): int
    {
        $price = Money::EUR($priceAmount);

        return $this->repository->addProduct($name, $price);
    }
}