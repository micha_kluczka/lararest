<?php

declare(strict_types=1);

namespace App\Modules\Product\DomainModel;

use Money\Money;

interface ProductRepository
{
    public function addProduct(string $name, Money $price): int;
}