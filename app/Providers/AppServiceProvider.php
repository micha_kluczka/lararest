<?php

namespace App\Providers;

use App\Modules\Product\DomainModel\ProductRepository;
use App\Modules\Product\Infrastructure\ProductMemoryRepository;
use App\Modules\Product\ReadModel\ProductReadModelRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ProductRepository::class, ProductMemoryRepository::class);
        $this->app->bind(ProductReadModelRepository::class, ProductMemoryRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
