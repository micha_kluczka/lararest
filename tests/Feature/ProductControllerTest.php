<?php

namespace Tests\Feature;

use Faker\Factory;
use Tests\TestCase;

class ProductControllerTest extends TestCase
{
    /**
     * @test
     */
    public function shouldCreateProduct(): void
    {
        $faker = Factory::create();

        $expectedName = $faker->sentence;
        $priceAmount = $faker->randomNumber(5, true);

        $payload = [
            'name' => $expectedName,
            'price' => $priceAmount
        ];

        $response = $this->post('/api/products', $payload);
        static::assertEquals(200, $response->getStatusCode());

        $decoded = json_decode($response->getContent(), true);
        static::assertEquals($expectedName, $decoded['name']);
        static::assertEquals($priceAmount, $decoded['price']['amount']);
        static::assertEquals('EUR', $decoded['price']['currency']);
        static::assertLessThanOrEqual(20, $decoded['id']);
        static::assertGreaterThanOrEqual(1, $decoded['id']);
    }
}
